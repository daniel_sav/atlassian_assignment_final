# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary :  Automation tests to create a confluence page and set restrictions on confluence page 
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Java TestNG Selenium project  data driven from a excel sheet where user input is required  .
There are 2 test in this framework
1)To Login and create a page on confluence(data provided through excel sheet)
2)To set up restrictions on confluence  page(data provided through excel sheet)




* Configuration

1)Unzip the atlassian assignment to your machine 

2)Check if java and chromebrowser is present on machine if not please install java(jdk 1.7 and jre) and install chrome browser.(The assignment runs with chromedriver)

3)Enter the atlassian  website,username,password and page to be created in the Inputs-Atlassian-assignment excel sheet tab pages like below.(I have already filled the excel sheet but these details will be invalid from Dec 7 ,please add valid details in the sheet )
 Atlassian website UserName	 Password	Page







4)Enter the below details  in the Inputs-Atlassian-assignment (present in the unzipped folder) excel sheet in tab restrictions like below.
  Atlassian website	UserName	Password	Page	Full name of the person needed to be given access to the page	Access needed

5)Once the above 4 steps are done ,save and close the sheet and double click on Atlassian-assignment.bat file and automation test case will run

6)I have put some waits in the logic so sometimes it may take for an action to occur,please wait till the action occurs.(take care not to open any other browser simultaneoulsy when the tests are running)


* Dependencies
1)Automation test will run on chrome so chrome browser needs to be installed and java needs to be installed as mentioned in the configuration
2)Valid Inputs in the excel sheet Inputs-Atlassian-assignment.xlsx needs to be provided in both the pages and Restrictions tabs before running the test 
3)Full name of the users  should be mentioned in the tab Restrictions to whom access for a page needs to be provided.

4)The details in the sheet  Inputs-Atlassian-assignment.xlsx will be invalid from Dec 7,please add your valid details to the sheet in both the tabs before running the bat file.

* How to run tests


 1)Double click on the bat file Atlassian-assignment.bat present in the unzipped assignment folder.
 2)You can view the results on the test-output folder and there will be a  report created called email

* Deployment instructions
When deploying on any machine,follow the steps mentioned in configuration . 




### Who do I talk to? ###

* Repo owner or admin
Daniel Fernandes,mobile -0414441509,email - danferns37@gmail.com